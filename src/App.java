import javax.lang.model.util.ElementScanner14;
import java.util.Arrays;
import java.util.regex.Pattern;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Task 50.30");

        // 1 Kiểm tra giá trị truyền vào có phải chuỗi hay không
        String a1 = "Devcamp";
        // checking for String
        if (a1.matches("\\d+")) {
            System.out.println(false);
        } else if (a1.matches("\\d*[.]\\d+")) {
            System.out.println(false);
        } else
            System.out.println(true);

        String a2 = "1";
        // checking for String
        if (a2.matches("\\d+")) {
            System.out.println(false);
        } else if (a2.matches("\\d*[.]\\d+")) {
            System.out.println(false);
        } else
            System.out.println(true);

        // 2 Cắt n phần tử đầu tiên của chuỗi
        String str = "Robin Singh";
        String[] arrOfStr = str.split("n");
        System.out.println("2.After Split: " + arrOfStr[0]);

        // 3-2 Chuyển chuỗi thành mảng các từ trong chuỗi
        // declaring and initializing a string
        String a3 = "Robin Singh";
        // passing the string to String[] {}
        String[] strArray = new String[] { a3 };
        // printing the string array using Arrays.toString()
        System.out.println("3-1. Convert to String Array" + Arrays.toString(strArray));

        // 3-1 Chuyển chuỗi thành mảng các từ trong chuỗi
        String line = "Robin Singh";
        // using String split function
        String[] words = line.split(" ");
        System.out.println(Arrays.toString(words));
        // using java.util.regex Pattern
        Pattern pattern = Pattern.compile(" ");
        words = pattern.split(line);
        System.out.println("3-2. Convert to Array" + Arrays.toString(words));

        // 4. Chuyển chuỗi về định dạng như output
        String txt = "Hello World";
        System.out.println("4. String after replace to lower case " + txt.toLowerCase());
        String newStr = txt.toLowerCase();
        System.out.println("4. String after replace to - : " + replaceSpace(newStr));

        // 5. Bỏ các khoảng trắng giữa các từ trong chuỗi, viết hoa chữ cái đầu tiên của
        // mỗi từ
        String txt5 = "JavaScript exercises"; // create a string
        // stores each characters to a char array
        char[] charArray = txt5.toCharArray();
        boolean foundSpace = true;

        for (int i = 0; i < charArray.length; i++) {

            // if the array element is a letter
            if (Character.isLetter(charArray[i])) {

                // check space is present before the letter
                if (foundSpace) {

                    // change the letter into uppercase
                    charArray[i] = Character.toUpperCase(charArray[i]);
                    foundSpace = false;
                }
            }

            else {
                // if the new character is not character
                foundSpace = true;
            }
        }

        // convert the char array to the string
        String newStr5 = String.valueOf(charArray);
        System.out.println("5. String after Capitalize the first character of each word in a String: " + newStr5);
        // String newStr5 = txt5.toUpperCase();
        System.out.println("5. String after remove space : " + newStr5.replaceAll("\\s", ""));

        // 6. Viết hoa chữ cái đầu tiên mỗi từ
        String txt6 = "js string exercises"; // create a string
        // stores each characters to a char array
        char[] charArray6 = txt6.toCharArray();
        boolean foundSpace6 = true;

        for (int i = 0; i < charArray6.length; i++) {

            // if the array element is a letter
            if (Character.isLetter(charArray6[i])) {

                // check space is present before the letter
                if (foundSpace6) {

                    // change the letter into uppercase
                    charArray6[i] = Character.toUpperCase(charArray6[i]);
                    foundSpace6 = false;
                }
            }

            else {
                // if the new character is not character
                foundSpace6 = true;
            }
        }

        // convert the char array to the string
        String newStr6 = String.valueOf(charArray6);
        System.out.println("6. String after Capitalize the first character of each word in a String: " + newStr6);

        // 7 Lặp lại n lần từ 1 từ cho trước, có khoảng trắng giữa các lần lặp
        String txt7 = "Ha!";
        String newtxt7 = txt7 + " ";
        String repeated = newtxt7.repeat(3);
        System.out.println("7. Repeat string: " + repeated);

        // 8 Đưa chuỗi thành mảng mà mỗi phần tử là n ký tự lần lượt từ chuỗi đó
        String str8 = "dcresource"; // Given String

        // Creating array of string length
        char[] arr = new char[str8.length()];

        // Copy character by character into array
        for (int i = 0; i < str8.length(); i++) {
            arr[i] = str8.charAt(i);
        }

        // Printing the character array
        for (char x : arr) {
            System.out.println(x);
        }

        // 8 Đưa chuỗi thành mảng mà mỗi phần tử là n ký tự lần lượt từ chuỗi đó
        StringBuilder sb = new StringBuilder("dcresource");
        for (int i = 2; i < sb.length(); i += 3)
            sb.insert(i, ' ');
        String sb1 = sb.toString();
        System.out.println("String after add space betwwen two characters: " + sb1);
        // using String split function
        String[] new8 = sb1.split(" ");
        System.out.println("8. Convert string to array: " + Arrays.toString(new8));

        // 9 Đếm số lần xuất hiện của chuỗi con trong 1 chuỗi cho trước
        String str9 = "The quick brown fox jumps over the lazy dog";
        String new9 = str9.toLowerCase();
        int count = (new9.split("the", -1).length) - 1;
        System.out.println("Total occurrences: " + count);

        //10. Thay n ký tự bên phải của một chuỗi bởi 1 chuỗi khác

    }

    // 4 Hàm thay thế space thành -
    public static String replaceSpace(String str) {
        String s = "";

        // Traverse the string character by character.
        for (int i = 0; i < str.length(); ++i) {

            // Changing the ith character
            // to '-' if it's a space.
            if (str.charAt(i) == ' ')
                s += '-';

            else
                s += str.charAt(i);
        }

        // return the modified string.
        return s;
    }

}
